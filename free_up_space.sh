#!/bin/bash
#do "chmod +x free_space.sh" to be able to execute the script
#to make the whole script run as root
[ "$UID" -eq 0 ] || exec sudo "$0" "$@"

user=leo

echo -e "\
\033[32m__         __\033[0m                                                       
\033[32m\ \       / _|\033[0m                                                        
\033[32m \ \     | |_\033[0m _ __ ___  ___   _   _ _ __    ___ _ __   __ _  ___ ___ 
\033[32m  > >    |  _\033[0m| '__/ _ \/ _ \ | | | | '_ \  / __| '_ \ / _' |/ __/ _ \ 
\033[32m / /     | | \033[0m| | |  __/  __/ | |_| | |_) | \__ \ |_) | (_| | (_|  __/
\033[32m/_/      |_| \033[0m|_|  \___|\___|  \__,_| .__/  |___/ .__/ \__,_|\___\___|
\033[32m ______      \033[0m                      | |         | |                   
\033[32m|______|     \033[0m                      |_|         |_|                   
"

#get the value of free disk space on the Linux partition
#use of the character "`" to be able to use "'" in the command
#df (disk filesystems) -h (human readable) -t (type) -BM (block size in mega)
initial_free_space=`df -BM -t ext4`
#-n (to avoid a new line) -e (to interpret the new line character)
echo -n "Total space on the Linux partition: "
#the next line cuts the echo output with a space delimiter (-d ' ') and outputs the field n°13 (-f13)
echo $initial_free_space | cut -d ' ' -f13
echo -e -n "\nSpace initially available on the Linux partition: "
echo -n $initial_free_space | cut -d ' ' -f15

echo -e "\n\033[32m***** Start of the cleanup *****\033[0m"
#du estimate file space usage -h (human readable) -s (summarize)
trash_space=`du -hs /home/$user/.local/share/Trash`
echo -n "Emptying the trash: "
echo $trash_space | cut -d ' ' -f1
rm -rf /home/$user/.local/share/Trash/*

telechargements_space=`du -hs /home/$user/Téléchargements`
echo -n "Emptying the Téléchargements folder: "
echo $telechargements_space | cut -d ' ' -f1
rm -rf /home/$user/Téléchargements/*

downloads_space=`du -hs /home/$user/Downloads`
echo -n "Emptying the Downloads folder: "
echo $downloads_space | cut -d ' ' -f1
rm -rf /home/$user/Downloads/*

logs_space=`du -hs /var/log/journal`
echo -n "Emptying the logs: "
echo $logs_space | cut -d ' ' -f1
rm -rf /var/log/journal/*

apt_cache_space=`du -hs /var/cache/apt`
echo -n "Cleaning up the apt cache: "
echo $apt_cache_space | cut -d ' ' -f1
apt-get clean

home_cache_space=`du -hs /home/$user/.cache/`
echo -n "Cleaning up the home cache: "
echo $home_cache_space | cut -d ' ' -f1
rm -rf /home/$user/.cache/*

#command > /dev/null: redirects the output of command (stdout) to /dev/null
#2>&1: redirects stderr to stdout, so errors (if any) also goes to /dev/null
echo -e "Cleaning up unnecessary packages..."
apt-get -y autoremove > /dev/null 2>&1

echo -e "\033[32m***** End of the cleanup *****\033[0m"
echo -e -n "\nSpace eventually available on the Linux partition: "
final_free_space=`df -BM -t ext4`
echo $final_free_space | cut -d ' ' -f15
