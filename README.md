![](images_readme/free_up_space_ascii_art.png)

_Author : Léo Chauvin_

## Description
The bash script free_up_space.sh allows to save some space on the Linux partition doing these actions:

* Emptying the trash

* Emptying the Téléchargements folder

* Emptying the Downloads folder

* Emptying the logs

* Cleaning up the apt cache

* Cleaning up the home cache

* Cleaning up unnecessary packages

For each case, it shows the space used by these elements before emptying/cleaning them.

## Requirements
To be able to run this program, you need to open free_up_space.sh and change the username line 6.

## Run the program
First, you need to make the script executable.

```bash
chmod +x free_up_space.sh
```

Then, you just have to execute it.

```bash
./free_up_space.sh
```
